<?php

    /* Sécurisaion du mot de passe */
    function secure($password)
    {
        define('PREFIX_SALT', 'cedric');
        define('SUFFIX_SALT', 'fami-pronz-booksifa');
        $secure = md5(PREFIX_SALT.$password.SUFFIX_SALT);
        return $secure;
    }

    /**
     * permet de lier l'instanciation de la casse avec les fichiers présents dans le dossier 'classes'
     * ----------------
     * s'exécute avec "spl_autoload_register('chargerClasse')"
     *
     * @param object $classe
     * @return void
     */
    function chargerClasse($classe)
    {
        require '../classes/' . $classe . '.php';
    }


?>