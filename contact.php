<?php 
    session_start();
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Accueil | Books'IFA by Cédric FAMIBELLE-PRONZOLA</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link rel="icon" href="./ifa/icon.ico" />
        <link href="./script/css/bootstrap.min.css" rel="stylesheet">
        <link href="./script/css/mdb.min.css" rel="stylesheet">
        <link href="./script/css/style.css" rel="stylesheet">
    </head>
    <body>

        <?php
            include './menu.php';
            
            if(!isset($_COOKIE['connectToBook']))
            {
                include 'log_sign.html';
            }

            use PHPMailer\PHPMailer\PHPMailer;
            use PHPMailer\PHPMailer\Exception;

            if(isset($_SESSION['first_name']))

            if(isset($_POST['send_mail']))
            {

                if(isset( $_POST['name']))
                $name = $_POST['name'];

                if(isset( $_POST['email']))
                $email = $_POST['email'];

                if(isset( $_POST['message']))
                $message = $_POST['message'];

                if(isset( $_POST['subject']))
                $subject = $_POST['subject'];

                require_once 'PHPMailer/Exception.php';
                require_once 'PHPMailer/PHPMailer.php';
                require_once 'PHPMailer/SMTP.php';
        
                $mail = new PHPMailer();
                // Config
                $mail->isSMTP();	// On utilise le protocole SMTP
                $mail->CharSet = 'UTF-8';
                $mail->Encoding = 'base64';
                $mail->Host = 'SSL0.OVH.NET';	// On utilise le serveur OVH
                $mail->SMTPAuth = true;
                $mail->Username = 'famibellepronzola.cedric@stagiairesifa.fr';	// Vos identifiants OVH
                $mail->Password = '19771102*';
                $mail->SMTPSecure = 'tls';	// On active l'encryptage TLS.
                $mail->Port = 587;	// Le port sur lequel se connecter
        
                // Info du mail
                $mail->setFrom($email, $name);	// L'adresse mail de l'envoyeur (vous) et le nom (optionnel)
                $mail->addAddress('c.pronzola@live.fr');	// Le(s) destinataire(s)
        
                $mail->isHTML(true);	// On utilise du HTML
                $mail->Subject = " [Contact Book's IFA ] " . html_entity_decode($subject);
                $mail->Body = html_entity_decode($message);
        
                $mail->send();	// On envoi le mail
        
            }

        ?>

        <div class="container">
            <section class="mb-4">
                <h2 class="h1-responsive font-weight-bold text-center my-4">Nous contacter</h2>
                <p class="text-center w-responsive mx-auto mb-5">
                    Une question ? N'hésitez pas à nous contacter. Notre équipe vous répondra dans les plus brefs délais.
                </p>

                <div class="row">
                    <div class="col-md-9 mb-md-0 mb-5">
                        <form id="contact-form" name="contact-form" action="contact.php" method="POST">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="md-form mb-0">
                                        <input <?php if(isset($_SESSION['first_name']) && isset($_SESSION['last_name'])) echo 'readonly'?> value="<?php if(isset($_SESSION['first_name']) && isset($_SESSION['last_name']))
                                            echo $_SESSION['first_name'] . ' ' . $_SESSION['last_name'] ; ?>" type="text" id="name" name="name" 
                                            class="form-control text-white" required> 
                                        <label for="name" class="">Votre nom</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="md-form mb-0">
                                        <input <?php if(isset($_SESSION['first_name']) && isset($_SESSION['last_name'])) echo 'readonly'?> value="<?php if(isset($_SESSION['email'])) 
                                            echo $_SESSION['email']?>" type="text" id="email" name="email" class="form-control text-white" required>
                                        <label for="email" class="">Votre adresse mail</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="md-form mb-0">
                                        <input type="text" id="subject" name="subject" class="form-control text-white" required>
                                        <label for="subject" class="">Objet du message</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="md-form">
                                        <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea text-white" required></textarea>
                                        <label for="message">Votre message</label>
                                    </div>

                                </div>
                            </div>
                        

                            <div class="text-center text-md-left">
                                <button <?php if(isset($_SESSION['email']) AND $_SESSION['email'] == 'admin@admin.fr') echo 'disabled' ?> class="btn btn-primary" type="submit" name="send_mail"><?php if(isset($_SESSION['email']) AND $_SESSION['email'] == 'admin@admin.fr'){ echo 'Vous êtes administrateur';}else{echo 'Envoyer';} ?></button>
                            </div>
                        </form>
                        <div class="status"></div>
                    </div>
                    <div class="col-md-3 text-center">
                        <ul class="list-unstyled mb-0">
                            <li><i class="fas fa-map-marker-alt fa-2x"></i>
                                <p>4 rue Saint Michel, 57000 Metz</p>
                            </li>

                            <li><i class="fas fa-phone mt-4 fa-2x"></i>
                                <p>01 02 03 04 05 06</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>

        <script src="./script/js/jquery-3.3.1.min.js"></script>
        <script src="./script/js/popper.min.js"></script>
        <script src="./script/js/bootstrap.min.js"></script>
        <script src="./script/js/mdb.min.js"></script>
        <script src="./script/js/main.js"></script>
    </body>

</html>