<?php 

    session_start();

        /**
     * permet de lier l'instanciation de la casse avec les fichiers présents dans le dossier 'classes'
     * ----------------
     * s'exécute avec "spl_autoload_register('chargerClasse')"
     *
     * @param object $classe
     * @return void
     */
    function chargerClasse($classe)
    {
        require './classes/' . $classe . '.php';
    }

    include './connect/connect.php';            # les informations de connexion à la base de données
    spl_autoload_register('chargerClasse');     # permet de charger les classes

    $db = new PDO ("mysql:host=$server;dbname=$dbname;charset=utf8", $user, $pass_db);

    $usersManager = new UsersManager($db);
    $commandContentManager = new CommandContentManager($db);
    $booksManager = new BooksManager($db);

    if(isset($_COOKIE['connectToBook']))
    {
        $connected = true;
        $user_random = $_COOKIE['connectToBook'];
        $user = $usersManager->getByRandom($user_random);
        $_SESSION['user_id'] = $user->user_id();
        $_SESSION['first_name'] = $user->first_name();
        $_SESSION['last_name'] = $user->last_name();
        $_SESSION['email'] = $user->email();
        $_SESSION['phone'] = $user->phone();
        $_SESSION['address'] = $user->address();
        $_SESSION['zip_code'] = $user->zip_code();
        $_SESSION['city'] = $user->city();
        setcookie('booksifa', null, -1, "/");
    }
    else
    {
        $connected = false;
    }


?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Administration | Books'IFA by Cédric FAMIBELLE-PRONZOLA</title>
        <link rel="icon" href="./ifa/icon.ico" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link href="./script/css/bootstrap.min.css" rel="stylesheet">
        <link href="./script/css/mdb.min.css" rel="stylesheet">
        <link href="./script/css/style.css" rel="stylesheet">
    </head>
    <body>

    <?php

        include 'menu.php';

    ?>
        <div class="container-fluid">
    <?php
    
        if($connected AND $_SESSION['first_name'] == 'admin' AND $_SESSION['last_name'] == 'admin' AND $_SESSION['email'] == 'admin@admin.fr')
        {
            echo '<h1 class="text-center mt-3">Page d\'administration</h1>';
            $access = true;
            $allBooks = $booksManager->getListNoOrder();
        }
        else
        {
            include 'log_sign.html';
            echo '<p>Accès réservé à l\'administrateur.<p>';
            $access = false;
        }

        if($access)
        {
    ?>
                <form name="formJson" id="formJson">
                    <button type="submit" name="download" id="createJsonFile" class="btn btn-success btn-md mb-5 rounded-pill">MAJ du JSON avant téléchargement</button>
                </form>
                <div id="toDownloadJSON" style="display:none;" class="mb-5">
                    <h2><a id="downloadJsonFile" download="all_books.json" href="./api/all_books.json"><i class="fas fa-file-download"></i> Télécharger le fichier JSON</a></h2>
                </div>


            <h2>Liste des Livres</h2>
        <div class="table-responsive">
            <table class="table table-bordered w-auto table-sm">
                <thead class="bg-success text-white text-center">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Titre</th>
                        <th scope="col">Description</th>
                        <th scope="col">Date</th>
                        <th scope="col">Edition</th>
                        <th scope="col">Catégorie</th>
                        <th scope="col">Format</th>
                        <th scope="col">Prix</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    for($i = 0; $i < count($allBooks); $i++)
                    {
                ?>
                        <tr class="text-danger text-center">
                            <td><?php echo $allBooks[$i]->book_id() ?></td>
                            <td><?php echo $allBooks[$i]->title() ?></td>
                            <td class="text-break w-50"><?php echo $allBooks[$i]->description() ?></td>
                            <td><?php echo $allBooks[$i]->release_date() ?></td>
                            <td><?php echo $allBooks[$i]->edition() ?></td>
                            <td><?php echo $allBooks[$i]->category() ?></td>
                            <td><?php echo $allBooks[$i]->format() ?></td>
                            <td><?php echo $allBooks[$i]->price() . ' €' ?></td>
                        </tr>
                <?php
                    }
                ?>

                </tbody>
            </table>
        </div>


    <?php      
        }

        if($access == true AND isset($_POST['download']))
        {
            $jsonBooks = json_encode($allBooks, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

            if(file_exists('./api/all_books.json'))
            {
                $file = file_get_contents('./api/all_books.json');
                $jsonFile = json_decode($file);

                file_put_contents('./api/all_books.json', $jsonBooks);
            }
            else
            {
                $open = fopen('./api/all_books.json', 'w');
                $write = fwrite($open, $jsonBooks);
            }

            $download = true;

        }

    ?>

</div>
        <script src="./script/js/jquery-3.3.1.min.js"></script>
        <script src="./script/js/popper.min.js"></script>
        <script src="./script/js/bootstrap.min.js"></script>
        <script src="./script/js/mdb.min.js"></script>
        <script src="./script/js/main.js"></script>
    </body>

</html>