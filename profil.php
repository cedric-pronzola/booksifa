<?php 

    session_start();

    /**
     * permet de lier l'instanciation de la casse avec les fichiers présents dans le dossier 'classes'
     * ----------------
     * s'exécute avec "spl_autoload_register('chargerClasse')"
     *
     * @param object $classe
     * @return void
     */
    function chargerClasse($classe)
    {
        require './classes/' . $classe . '.php';
    }

    /* Sécurisaion du mot de passe */
    function secure($password)
    {
        define('PREFIX_SALT', 'cedric');
        define('SUFFIX_SALT', 'fami-pronz-booksifa');
        $secure = md5(PREFIX_SALT.$password.SUFFIX_SALT);
        return $secure;
    }

    include './connect/connect.php';            # les informations de connexion à la base de données
    spl_autoload_register('chargerClasse');     # permet de charger les classes

    $db = new PDO ("mysql:host=$server;dbname=$dbname;charset=utf8", $user, $pass_db);

    $usersManager = new UsersManager($db);
    $booksManager = new BooksManager($db);
    $authorsManager = new AuthorsManager($db);
    $commandsManager = new CommandsManager($db);
    $commandContentManager = new CommandContentManager($db);

    if(isset($_COOKIE['connectToBook']))
    {
        $connected = true;
        $user_random = $_COOKIE['connectToBook'];
        $user = $usersManager->getByRandom($user_random);
    }
    else
    {
        $connected = false;
    }


?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php if($connected) echo $user->first_name() ?> | Books'IFA by Cédric FAMIBELLE-PRONZOLA</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link rel="icon" href="./ifa/icon.ico" />
        <link href="./script/css/bootstrap.min.css" rel="stylesheet">
        <link href="./script/css/mdb.min.css" rel="stylesheet">
        <link href="./script/css/style.css" rel="stylesheet">
    </head>
    <body>

        <?php

            include 'menu.php';
            if(!$connected)
            {
                echo "<p>Accès refusé. vous n'êtes pas connecté</p>";
                include 'log_sign.html';
            }
            elseif($_SESSION['email'] == 'admin@admin.fr')
            {
                echo "Accès admin en cours de construction";
            }
            else
            {
                $confirmation = "return confirm(\"Voulez-vous modifier vos informations personnelles ?\")";
        ?>
        <div class="container-fluid">

            <?php

                if($commandsManager->get($user->user_id()) === NULL)
                {
                    echo "<h3>Aucune commande</h3>";
                    
                }
                else
                {
                    echo '<h1>Mes Commandes</h1>';

                $myCommands = $commandsManager->get($user->user_id());
            ?>

                <table class="table table-hover table-dark">
                    <thead>
                        <tr class="bg-primary">
                            <th scope="col">Commande</th>
                            <th scope="col">Date</th>
                            <th scope="col">Prix Total</th>
                            <th scope="col">Mode de paiement</th>
                            <th scope="col">Détails</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php

                    $det = '';
                    
                        for($i = 0; $i < count($myCommands); $i++)
                        {
                            
                    ?>
                        <tr>
                            <td><?php echo $myCommands[$i]['num_command'] ?></td>
                            <td><?php echo date('d-m-Y', strtotime($myCommands[$i]['date_command'])) ?></td>
                            <td><?php echo $myCommands[$i]['total_price'] . ' €' ?></td>
                            <td><?php echo $myCommands[$i]['mode'] ?></td>
                            <td>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="<?php echo '#mod'.$myCommands[$i]['num_command'] ?>">
                                    <i class="fas fa-plus-circle"></i>
                                </button>
                            </td>
                        </tr>
                    <?php
        
                        }
                    ?>

                    </tbody>
                </table>
                        <?php
                            for($j = 0; $j < count($myCommands); $j++)
                            {
                        ?>
                                <div class="modal fade right" id="<?php echo 'mod'.$myCommands[$j]['num_command'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog modal-full-height modal-right" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title text-primary" id="exampleModalLabel">Commande n° <?php echo $myCommands[$j]['num_command'] ?></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body text-danger">
                                                <?php

                                                    $myTab = $commandContentManager->getNumCommand($myCommands[$j]['num_command']);
                                                ?>
                                                <table class="table table-sm">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Livre</th>
                                                        <th scope="col">Quantité</th>
                                                        <th scope="col">Prix unitaire</th>
                                                        <th scope="col">Fiche</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                <?php
                                                   for($k = 0; $k < count($myTab); $k++)
                                                   {
                                                ?>
                                                    <tr>
                                                        <th><?php echo $booksManager->get($myTab[$k]['book_id'])->title()?></th>
                                                        <th><?php echo $myTab[$k]['quantity']?></th>
                                                        <th><?php echo $booksManager->get($myTab[$k]['book_id'])->price() . ' €'?></th>
                                                        <th><a href="./pages/fiches.php?book_id=<?php echo $myTab[$k]['book_id']?>"><i class="fas fa-book"></i></a></th>
                                                        
                                                    </tr>

                                                <?php
                                                   }
                                                ?>
                                                        </tbody>
                                                    </table>
                                                <?php

                                                ?>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?php
                            }        
                }
                        ?>

            <h1>Modifier mes informations personnelles</h1>

            <form action="" method="POST" class="container-fluid">
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="validationDefault01">Prénom</label>
                        <input name="first_name" type="text" class="form-control" id="validationDefault01" value="<?php echo $user->first_name() ?>" required>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationDefault02">Nom</label>
                        <input name="last_name" type="text" class="form-control" id="validationDefault02" value="<?php echo $user->last_name() ?>" required>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationDefault04">Numéro de téléphone</label>
                        <input name="phone" type="tel" class="form-control" id="validationDefault04" value="<?php echo $user->phone() ?>" required
                        pattern="(0|\+33)[1-9]( *[0-9]{2}){4}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="validationDefaultUsername">Adresse Mail </label>
                        <input name="email" type="email" class="form-control" id="validationDefaultUsername" aria-describedby="inputGroupPrepend2" value="<?php echo $user->email() ?>" required
                        pattern="[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([_\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label for="validationDefault03">Adresse</label>
                        <input name="address" type="text" class="form-control" id="validationDefault03" value="<?php echo $user->address() ?>" required>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationDefault05">Ville</label>
                        <input name="city" type="text" class="form-control" id="validationDefault05" value="<?php echo $user->city() ?>" required>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationDefault15">Code postal</label>
                        <input name="zip_code" type="text" class="form-control" id="validationDefault15" value="<?php echo $user->zip_code() ?>" required>
                    </div>
                </div>
                <button onclick="return confirm('Voulez-vous modifier vos informations personnelles ?')" name="edit_profil" class="btn btn-primary" type="submit">Valider les modifications</button>
            </form>

        <?php        
            }
            if(isset($_POST['edit_profil']))
            {
                $first_name = $_POST['first_name'];
                $last_name = $_POST['last_name'];
                $phone = $_POST['phone'];
                $email = $_POST['email'];
                $address = $_POST['address'];
                $city = $_POST['city'];
                $zip_code = $_POST['zip_code'];

                $user->setFirst_name($first_name);
                $user->setLast_name($last_name);
                $user->setPhone($phone);
                $user->setEmail($email);
                $user->setAddress($address);
                $user->setCity($city);
                $user->setZip_code($zip_code);

                $usersManager->update($user);

                echo '<script type="text/javascript">
				      document.location.href="./profil.php";
                    </script>';
            }
        ?>

        </div>      
        <script src="./script/js/jquery-3.3.1.min.js"></script>
        <script src="./script/js/popper.min.js"></script>
        <script src="./script/js/bootstrap.min.js"></script>
        <script src="./script/js/mdb.min.js"></script>
        <script src="./script/js/main.js"></script>
    </body>
</html>