## Disponible sur : http://www.cedric-famibelle.fr/librairie-poo

# Librairie POO

* Un client, libraire, vous contact pour réaliser son site internet. 
* Il souhaite proposer la vente en ligne de ses produits. 
* Vous allez donc développer un site e-commerce de livres. 

[![Gif Books'IFA]( http://www.cedric-famibelle.fr/booksifa/booksifa.gif)](https://www.youtube.com/watch?v=qj-Xvikrsw0)
