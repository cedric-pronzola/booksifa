<?php
    session_start();

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require_once 'PHPMailer/Exception.php';
    require_once 'PHPMailer/PHPMailer.php';
    require_once 'PHPMailer/SMTP.php';

    include '../connect/connect.php';               # les informations de connexion à la base de données
    include '../connect/functions.php';             # regroupe les fonctions 
    spl_autoload_register('chargerClasse');         # permet de charger les classes

    $db = new PDO ("mysql:host=$server;dbname=$dbname;charset=utf8", $user, $pass_db);

    $usersManager = new UsersManager($db);
    $booksManager = new BooksManager($db);
    $authorsManager = new AuthorsManager($db);
    $commandsManager = new CommandsManager($db);
    $commandContentManager = new CommandContentManager($db);





?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Passer commande</title>
        <link rel="icon" href="../ifa/icon.ico" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link href="../script/css/bootstrap.min.css" rel="stylesheet">
        <link href="../script/css/mdb.min.css" rel="stylesheet">
        <link href="../script/css/style.css" rel="stylesheet">
    </head>
    <body>

        <?php

            include './menu.php';
        ?>
        <div class="container-fluid">
        <?php
            if(!isset($_COOKIE['connectToBook']))
            {
                include 'log_sign.html';
                echo '<p>Accès refusé</p>';
            }
            elseif($_COOKIE['inCartItemsNum'] == 0)
            {
                echo '<h2>Aucun article dans le panier</h2>';
            }
            else
            {
                $user_id = $_SESSION['user_id'];
                $user_random = $usersManager->getByRandom($_COOKIE['connectToBook']);
                $user_id_from_random = $user_random->user_id();
                $jsonFile = json_decode($_COOKIE['cartArticles']);

                if(isset($_POST['confirm-command']))
                {

                    $myCommands = json_decode($_COOKIE['cartArticles']);
                    $allCommands = $commandsManager->myCommands($myCommands, $user_id_from_random);

                    ?>

                        <h1>Confirmation de la commande</h1>

                        <table class="table table-hover table-dark">
                            <thead class="bg-success">
                                <tr>
                                    <th scope="col">Titre du livre</th>
                                    <th scope="col">Quantité</th>
                                    <th scope="col">Prix à l'unité</th>
                                    <th scope="col">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                    <?php
                        $price = 0;
                        for($i = 0; $i < count($allCommands); $i++)
                        {
                            $price += $booksManager->get($allCommands[$i]['bookId'])->price() * $allCommands[$i]['qt'];

                    ?>  
                            <tr>
                                <th scope="row"><?php echo $booksManager->get($allCommands[$i]['bookId'])->title() ?></th>
                                <td><?php echo $allCommands[$i]['qt'] ?></td>
                                <td><?php echo $booksManager->get($allCommands[$i]['bookId'])->price() ?> €</td>
                                <td><?php echo $booksManager->get($allCommands[$i]['bookId'])->price() * $allCommands[$i]['qt'] ?> €</td>
                            </tr>

                    <?php
                        }
                    ?>
                            </tbody>
                        </table>

                        <h2>Prix Total </h2>
                        <h2 class="text-success"><?php echo $price ?> €</h2>

                        <div class="card text-white bg-success mb-3 mt-1" style="max-width: 18rem;">
                            <div class="card-header text-center">Adresse de livraison</div>
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $usersManager->get($user_id)->first_name() . ' ' .  $usersManager->get($user_id)->last_name()?></h5>
                                <p class="card-text text-white"><?php echo $usersManager->get($user_id)->address() ?></p>
                                <p class="card-text text-white"><?php echo $usersManager->get($user_id)->zip_code() ?></p>
                                <p class="card-text text-white"><?php echo $usersManager->get($user_id)->city() ?></p>
                            </div>
                        </div>

                        <form action="" method="POST" class="form-inline">
                            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Mode de paiement </label>
                            <select name="mode" class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
                                <option value="Carte">Carte</option>
                                <option value="Paypal">PayPal</option>
                                <option value="Bitcoin">Bitcoin</option>
                            </select>

                            <button type="submit" name="paied" id="paied" class="btn btn-primary my-1">Payer</button>
                        </form>
                    <?php
                }

                if(isset($_POST['paied']))
                {
                    $commandeOk = false;
                    $myPaiedCommands = json_decode($_COOKIE['cartArticles']);
                    $mode = $_POST['mode'];

                    $allPaiedCommands = $commandsManager->myPaiedCommands($myPaiedCommands, $user_id_from_random, $mode);

                    $price = 0;

                    for($i = 0; $i < count($allPaiedCommands); $i++)
                    {
                        $price += $booksManager->get($allPaiedCommands[$i]['bookId'])->price() * $allPaiedCommands[$i]['qt'];
                    }

                    $donnees = [];
                    $donnees2 = [];

                    $donnees = array(
                        'user_id' => $allPaiedCommands[0]['userId'],
                        'date_command' => date('Y-m-d'),
                        'total_price' => $price,
                        'random' => $allPaiedCommands[0]['random'],
                        'mode' => $allPaiedCommands[0]['mode']
                    );

                    $random = $donnees['random'];
                    $_SESSION['random'] = $random;
                    $_SESSION['user_id'] = $user_id_from_random;
                    $newCommand = new Commands($donnees);
                    $commandsManager->add($newCommand);

                    $result = $commandsManager->getByUserId($donnees['user_id'], $random, $donnees['total_price']);
                    $numForCommandContent = $result->num_command();

                    for($i = 0; $i < count($allPaiedCommands); $i++)
                    {
                        $donnees2 = array(
                            'num_command' => (int) $numForCommandContent,
                            'book_id' => (int) $allPaiedCommands[$i]['bookId'],
                            'quantity' => (int) $allPaiedCommands[$i]['qt']
                        );

                        $newCommandContentManager = new CommandContent($donnees2);
                        $commandContentManager->add($newCommandContentManager);

                    }

                    $commandeOk = true;
                    $jsonMail = '';

                    for($i = 0; $i < count($jsonFile); $i++)
                    {
                        $jsonMail .= '<tr>' . 
                                        '<td>' . $jsonFile[$i]->name . '</td>' .
                                        '<td>' . $jsonFile[$i]->qt . '</td>' .
                                        '<td>' . $jsonFile[$i]->price . ' € </td>' .
                                        '<td>' . $jsonFile[$i]->price *  $jsonFile[$i]->qt . ' €</td>' .
                                    '</tr>';
                    }

                    if($commandeOk)
                    {
                        
                        $mail = new PHPMailer();
                        $mailBody = '<p>Bonjour ' . $user_random->first_name() . ', voici le récapitulatif de la commande n° ' . $donnees2['num_command'] . '</p>' . 
                                    '<p> Date : ' . date('d-m-Y', strtotime($donnees['date_command'])) . '</p>' .
                                    '<p> Prix total : ' . $donnees['total_price'] . ' euros </p>' .
                                    '<p> Mode de paiement : ' . $donnees['mode'] . '</p>' .
                                    '<p> Adresse de Livraison : </p>' . 
                                        $usersManager->get($user_id)->first_name() . ' ' . $usersManager->get($user_id)->last_name() . '<br>' .
                                        $usersManager->get($user_id)->address() . '<br>' .
                                        $usersManager->get($user_id)->zip_code() . '<br>' .
                                        $usersManager->get($user_id)->city() . '<br>' .
                                    '<h3>Récapitulatif</h3>'.
                                    '<table style="width:80%; border:2px solid black;">
                                        <thead>
                                            <tr>
                                                <th style="border:1px solid black;"> Titre du livre </th>
                                                <th style="border:1px solid black;"> Quantité </th>
                                                <th style="border:1px solid black;"> Prix </th>
                                                <th style="border:1px solid black;"> Total </th>
                                            </tr>
                                            ' . $jsonMail . '
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>';

                        $commandsManager->sendConfirmationMail($user_random, $mail, $numForCommandContent, $mailBody);

                        ?>
                            <script>
                                document.location.href="./confirmation.php";
                            </script>
                        <?php
                    }
                }
            }
                        ?>
        </div>
        <script src="../script/js/jquery-3.3.1.min.js"></script>
        <script src="../script/js/popper.min.js"></script>
        <script src="../script/js/bootstrap.min.js"></script>
        <script src="../script/js/mdb.min.js"></script>
        <script src="../script/js/main.js"></script>
    </body>
</html>