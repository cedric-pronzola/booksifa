<?php
    session_start();
    include '../connect/connect.php';               # les informations de connexion à la base de données
    include '../connect/functions.php';             # regroupe les fonctions 
    spl_autoload_register('chargerClasse');         # permet de charger les classes

    $db = new PDO ("mysql:host=$server;dbname=$dbname;charset=utf8", $user, $pass_db);

    $booksManager = new BooksManager($db);
    $authorsManager = new AuthorsManager($db);
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Livres | Books'IFA by Cédric FAMIBELLE-PRONZOLA</title>
        <link rel="icon" href="../ifa/icon.ico" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link href="../script/css/bootstrap.min.css" rel="stylesheet">
        <link href="../script/css/mdb.min.css" rel="stylesheet">
        <link href="../script/css/style.css" rel="stylesheet">
    </head>
    <body>
        <?php

            include './menu.php';
            
            if(!isset($_COOKIE['connectToBook']))
            {
                include 'log_sign.html';
            }

        ?>
        <div class="container-fluid">
                <form action="" method="GET" class="d-flex justify-content-around form-inline border border-warning pb-1 mb-1">
                        <div class="form-group col-md-2 custom-control custom-checkbox ml-3 mt-1 form-check-inline">
                            <input type="checkbox" name="format[]" value="grand_format" class="custom-control-input 1" id="grand_format" value="grand_format">
                            <label class="custom-control-label" for="grand_format">Grand Format</label>
                        </div>
                        <div class="form-group col-md-2 custom-control custom-checkbox ml-3 mt-1 form-check-inline">
                            <input type="checkbox" name="format[]" value="poche" class="custom-control-input 2" id="poche" value="poche">
                            <label class="custom-control-label" for="poche">Poche</label>
                        </div>
                        <div class="form-group col-md-2 mt-1">
                            <label class="mr-2" for="sort_by">Trier par </label>
                            <select name="sort" class="custom-select" id="sort_by">
                                    <option value="release_date">Date de sortie</option>
                                    <option value="name">Auteur</option>
                                    <option value="title">Titre</option>
                                    <option value="price">Prix</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2 mt-1">
                            <label class="mr-2" for="order">Ordre</label>
                            <select name="order" class="custom-select" id="order">
                                    <option value="ASC">Croissant</option>
                                    <option selected value="DESC">Décroissant</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2 mt-1">
                            <label class="mr-2" for="category">Categories </label>
                            <select name="category" class="custom-select" id="category">
                                <option value="all_cat" selected>Toutes</option>
                                <?php for($j = 0; $j < count($booksManager->categoryList()); $j++)
                                        {
                                ?>
                                            <option value="<?php echo $booksManager->categoryList()[$j]->category() ?>"> 
                                                <?php echo $booksManager->categoryList()[$j]->category() ?>
                                            </option>
                                <?php   
                                        }
                                ?>
                            </select>
                        </div>
                        <button type="submit" name="submit" class="btn btn-danger btn-sm mt-2">Ok</button>
                </form>

            <?php

                $requestChoice = $booksManager->getList();

                $sort = '';
                $ranges ='';
                $cat = ''; 
                $format = '';

                if(isset($_GET['submit']))
                {
                    $sort = $_GET['sort'];
                    $order = $_GET['order'];
                    $category = $_GET['category'];

                    if(isset($_GET['format']))
                    {
                        $format = $_GET['format'];
                        $requestChoice = $booksManager->sortForm($format, $sort, $order, $category);
                    }
                    else
                    {
                        $format =[];
                        $requestChoice = $booksManager->sortForm($format, $sort, $order, $category);
                    }

                    if(isset($_GET['sort']))
                    {
                        $sort = $_GET['sort'];
                    }
                    
                }
            ?>
            <div class="d-flex justify-content-center row mt-3">

                <?php

                    for($i = 0; $i < count($requestChoice); $i++)
                    {
                ?>
                        <div class="card promoting-card col-2 col-sm-3 col-md-2 ml-3 mb-5">
                            <div class="card-body d-flex flex-row">
                                <div>
                                    <a data-toggle="tooltip" data-placement="bottom" title="<?php echo $requestChoice[$i]->title() ?>" href="./fiches.php?book_id=<?php echo $requestChoice[$i]->book_id() ?>"> <h5 style="max-width: 21vh;" class="card-title font-weight-bold mb-2 text-danger text-truncate"><?php echo $requestChoice[$i]->title() ?> </h5></a>
                                    <p class="card-text"><i class="fas fa-portrait pr-2"></i> <a href="./fiches_authors.php?author_id=<?php echo $authorsManager->getByBookId($requestChoice[$i]->book_id())->author_id()?>">
                                    <?php echo $authorsManager->getByBookId($requestChoice[$i]->book_id())->name() ?></a></p>
                                    <p class="card-text"><i class="fas fa-book pr-2"></i> <?php echo $requestChoice[$i]->format() ?></p>
                                    <p class="card-text"><i class="fas fa-calendar-alt pr-2"></i> <?php echo $requestChoice[$i]->release_date() ?></p>
                                    <p class="card-text text-success"><?php echo $requestChoice[$i]->price() ?> <i class="fas fa-euro-sign pr-2"></i></p>
                                </div>
                            </div>
                            <div class="view overlay">
                                <img style="max-width: 250px; max-height: 300px;" class="card-img-top rounded-0" src="../img/<?php echo $requestChoice[$i]->img() ?>" alt="Card image cap">
                                <a href="./fiches.php?book_id=<?php echo $requestChoice[$i]->book_id() ?>">
                                <div class="mask rgba-white-slight"></div>
                                </a>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <button type="button" data-id="<?php echo $requestChoice[$i]->book_id() ?>" data-name="<?php echo $requestChoice[$i]->title() ?>" data-price="<?php echo $requestChoice[$i]->price() ?>" data-weight="10" data-url="./fiches.php?book_id=<?php echo $requestChoice[$i]->book_id() ?>" class="add-to-cart fas fa-cart-plus fa-2x text-muted p-1 my-1 mr-3 rounded" title="Article ajouté"></button>
                                    <div class="def-number-input number-input">
                                        <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus"></button>
                                            <input class="qt quantity" min="1" name="quantity" value="1" type="number">
                                        <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus"></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                <?php    
                    }
                ?>
            </div>
            
            <script src="../script/js/jquery-3.3.1.min.js"></script>
            <script src="../script/js/popper.min.js"></script>
            <script src="../script/js/bootstrap.min.js"></script>
            <script src="../script/js/mdb.min.js"></script>
            <script src="../script/js/main.js"></script>
        </div>
    </body>

</html>