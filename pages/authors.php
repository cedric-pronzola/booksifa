<?php
    session_start();
    include '../connect/connect.php';               # les informations de connexion à la base de données
    include '../connect/functions.php';             # regroupe les fonctions 
    spl_autoload_register('chargerClasse');         # permet de charger les classes

    $db = new PDO ("mysql:host=$server;dbname=$dbname;charset=utf8", $user, $pass_db);

    $booksManager = new BooksManager($db);
    $authorsManager = new AuthorsManager($db);
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Auteurs | Books'IFA by Cédric FAMIBELLE-PRONZOLA</title>
        <link rel="icon" href="../ifa/icon.ico" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link href="../script/css/bootstrap.min.css" rel="stylesheet">
        <link href="../script/css/mdb.min.css" rel="stylesheet">
        <link href="../script/css/style.css" rel="stylesheet">
    </head>
    <body>
        <?php

        include './menu.php';

        if(!isset($_COOKIE['connectToBook']))
        {
            include 'log_sign.html';
        }

        ?>
        <div class="container-fluid">
            <table id="dtBasicExample" class="table table-hover table-dark table-bordered">
                <thead>
                    <tr class="bg-danger">
                        <th scope="col">Nom</th>
                        <th scope="col">Sexe</th>
                        <th scope="col">Date de naissance</th>
                        <th scope="col">Photo</th>
                        <th scope="col">Biographie</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        for($i = 0; $i < count($authorsManager->getList()); $i++)
                        {
                        ?>      
                            <tr>
                                <th scope="row"><?php echo $authorsManager->getList()[$i]->name() ?></th>
                                <td><?php echo $authorsManager->getList()[$i]->sexe() ?></td>
                                <td><?php echo $authorsManager->getList()[$i]->birth_date() ?></td>
                                <td>
                                    <img style="width: 150px" src="../img/<?php echo $authorsManager->getList()[$i]->img() ?>">
                                </td>
                                <td> 
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#bio<?php echo $i ?>">
                                        Cliquez ici
                                    </button>
                                    <div class="modal fade" id="bio<?php echo $i ?>" tabindex="-1" role="dialog" aria-labelledby="bio<?php echo $i ?>" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="text-dark modal-title" id="bio<?php echo $i ?>"><?php echo $authorsManager->getList()[$i]->name()?></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="text-dark modal-body">
                                                <?php echo $authorsManager->getList()[$i]->biography() ?>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>

        <script src="../script/js/jquery-3.3.1.min.js"></script>
        <script src="../script/js/popper.min.js"></script>
        <script src="../script/js/bootstrap.min.js"></script>
        <script src="../script/js/mdb.min.js"></script>
        <script src="../script/js/addons/datatables.js"></script>
        <script>
            $(document).ready(function () {
            $('#dtBasicExample').DataTable();
            $('.dataTables_length').addClass('bs-select');
            });
        </script>
    </body>
</html>