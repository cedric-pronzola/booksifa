<?php

    session_start();

    class verifMail extends Exception{};
    class verifPassword extends Exception{};
    class verifAccount extends Exception{};

    include '../connect/connect.php';
    include '../connect/functions.php';
    spl_autoload_register('chargerClasse');

    $db = new PDO ("mysql:host=$server;dbname=$dbname;charset=utf8", $user, $pass_db);

    $usersManager = new UsersManager($db);

    if(isset($_POST['sign_up']))
    {
        $sign = $_POST['sign_up'];

        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $email = $_POST['email'];
        $password = secure($_POST['password']);
        $phone = $_POST['phone'];
        $address = $_POST['address'];
        $zip_code = $_POST['zip_code'];
        $city = $_POST['city'];
        $country = $_POST['country'];
        $random = random_int(245343, 2753778677852);

        $donnees = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'password' => $password,
            'phone' => $phone,
            'address' => $address,
            'zip_code' => $zip_code,
            'city' => $city,
            'country' => $country,
            'random' => $random
        );

        $user = new Users($donnees);
        $usersManager->verifyAccount($user);

        header ('location: ../index.php');
    }

    if(isset($_POST['log_in']))
    {
        $email = $_POST['email'];
        $password = secure($_POST['password']);

        $usersManager->verifyEmail($email, $password);

        header ('location: ../index.php');
    }


?>