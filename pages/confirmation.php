<?php
    session_start();

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require_once 'PHPMailer/Exception.php';
    require_once 'PHPMailer/PHPMailer.php';
    require_once 'PHPMailer/SMTP.php';


    include '../connect/connect.php';               # les informations de connexion à la base de données
    include '../connect/functions.php';             # regroupe les fonctions 
    spl_autoload_register('chargerClasse');         # permet de charger les classes

    $db = new PDO ("mysql:host=$server;dbname=$dbname;charset=utf8", $user, $pass_db);

    $usersManager = new UsersManager($db);
    $booksManager = new BooksManager($db);
    $authorsManager = new AuthorsManager($db);
    $commandsManager = new CommandsManager($db);
    $commandContentManager = new CommandContentManager($db);

    setcookie('cartArticles', null, -1, "/");
    setcookie('inCartItemsNum', null, -1, "/");

?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Confirmation</title>
        <link rel="icon" href="../ifa/icon.ico" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link href="../script/css/bootstrap.min.css" rel="stylesheet">
        <link href="../script/css/mdb.min.css" rel="stylesheet">
        <link href="../script/css/style.css" rel="stylesheet">
    </head>
    <body>

    
        <?php

            include './menu.php';

            if(!isset($_COOKIE['connectToBook']) AND !isset($_SESSION['user_id']) AND !isset($_SESSION['random']))
            {
                include 'log_sign.html';
                echo '<p>Accès refusé</p>';
            }
            else
            {
            ?>
        <div class="container-fluid">
                <h1>Récapitulatif de votre commande</h1>
                
            <?php

                $commandRand = $_SESSION['random'];
                $user_id = $_SESSION['user_id'];
                $random_user = $_COOKIE['connectToBook'];

                $command = $commandsManager->getByRandom($user_id, $commandRand);
                $contenu_command = $commandContentManager->getNumCommand($command->num_command());
                $num_command = $command->num_command();
                $userObject = $usersManager->get($user_id);
            }

        ?>
    
            <div class="card text-white bg-danger mb-3" style="max-width: 18rem;">
                <div class="card-header">Date de commande : <?php echo date('d-m-Y', strtotime($command->date_command())) ?></div>
                <div class="card-body">
                    <h5 class="card-title">Prix Total : <?php echo $command->total_price() . ' €' ?></h5>
                    <p class="card-text text-dark">Numéro de commande : <?php echo $command->num_command() ?></p>
                    <p class="card-text text-dark">Mode de paiement : <?php echo $command->mode() ?></p>
                </div>
            </div>

            <h2>Détail de votre commande</h2>
            <div class="table-responsive-lg">
                <table class="table table-hover table-dark table-active">
                    <thead>
                        <tr>
                            <th class="bg-danger" scope="col">Titre du livre</th>
                            <th class="bg-danger" scope="col">Quantité</th>
                            <th class="bg-danger" scope="col">Prix </th>
                            <th class="bg-danger" scope="col">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                <?php
                        for($i = 0; $i < count($contenu_command); $i++)
                        {
                ?>
                        <tr>
                            <th scope="row"><?php echo $booksManager->get($contenu_command[$i]['book_id'])->title() ?></th>
                            <td><?php echo $contenu_command[$i]['quantity'] ?></td>
                            <td><?php echo $booksManager->get($contenu_command[$i]['book_id'])->price() . ' €'?></td>
                            <td><?php echo $booksManager->get($contenu_command[$i]['book_id'])->price() * $contenu_command[$i]['quantity'] . ' €' ?></td>
                        </tr>
                <?php
                        }

                ?>

                    </tbody>
                </table>
            </div>
        </div>


        <script src="../script/js/jquery-3.3.1.min.js"></script>
        <script src="../script/js/popper.min.js"></script>
        <script src="../script/js/bootstrap.min.js"></script>
        <script src="../script/js/mdb.min.js"></script>
        <script src="../script/js/main.js"></script>
    </div>
    </body>
</html>