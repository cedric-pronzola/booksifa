<?php
    session_start();
    include '../connect/connect.php';               # les informations de connexion à la base de données
    include '../connect/functions.php';             # regroupe les fonctions 
    spl_autoload_register('chargerClasse');         # permet de charger les classes

    $db = new PDO ("mysql:host=$server;dbname=$dbname;charset=utf8", $user, $pass_db);

    $booksManager = new BooksManager($db);
    $authorsManager = new AuthorsManager($db);
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Panier</title>
        <link rel="icon" href="../ifa/icon.ico" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link href="../script/css/bootstrap.min.css" rel="stylesheet">
        <link href="../script/css/mdb.min.css" rel="stylesheet">
        <link href="../script/css/style.css" rel="stylesheet">
    </head>
    <body>
            <?php

            include './menu.php';

            if(!isset($_COOKIE['connectToBook']))
            {
                include 'log_sign.html';
            }
            ?>
        <div class="container-fluid">

            <table class="table table-hover table-bordered table-dark">
                <thead class="black red-text">
                    <tr>
                        <th scope="col">Article</th>
                        <th scope="col">Prix</th>
                        <th scope="col">Quantité</th>
                        <th scope="col">Port</th>
                    </tr>
                </thead>
                <tbody id="cart-tablebody" class="text-success">
    
                </tbody>
            </table>

            <p>Total : <span class="subtotal"></span>€</p>
            <?php 
            
                if(isset($_COOKIE['cartArticles']) AND $_COOKIE['inCartItemsNum'] > 0)
                {

                    if(isset($_COOKIE['connectToBook']))
                    {
                        ?>
                        <form action="shop.php" method="post">
                            <p>
                                <button id="confirm-command" name="confirm-command">Passer la commande</button>
                            </p>
                        </form>
                                
                        <?php
                    }
                    else
                    {
                        ?>
                            <p>
                                <button disabled id="confirm-command" name="">Passer la commande</button>
                            </p>
                            <p>
                                Veuillez vous connecter pour passer commande.
                            </p>
                        <?php
                    }
                    ?>
                        <button type="button" id="del_all_cart">Vider le panier</button>
                    <?php
                }
            ?>

            <script src="../script/js/jquery-3.3.1.min.js"></script>
            <script src="../script/js/popper.min.js"></script>
            <script src="../script/js/bootstrap.min.js"></script>
            <script src="../script/js/mdb.min.js"></script>
            <script src="../script/js/main.js"></script>
        </div>    
    </body>
</html>
