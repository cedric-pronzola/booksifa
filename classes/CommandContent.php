<?php

    class CommandContent
    {
        private $_num_command;
        private $_book_id;
        private $_quantity;

        public function __construct(array $donnees)
        {
            $this->hydrate($donnees);
        }

        public function hydrate(array $donnees)
        {
            foreach($donnees as $key => $value)
            {
                $method = 'set'.ucfirst($key);
    
                if(method_exists($this, $method))
                {
                    $this->$method($value);
                }
            }
        }

        /**
         *  Getters
         */

        public function num_command()
        {
            return $this->_num_command;
        }
        public function book_id()
        {
            return $this->_book_id;
        }
        public function quantity()
        {
            return $this->_quantity;
        }

        /**
         *  Setters
         */
        public function setNum_command($num_command)
        {
            $num_command = (int) $num_command;
            if($num_command > 0)
            {
                $this->_num_command = $num_command;
            }
        }

        public function setBook_id($book_id)
        {
            $book_id = (int) $book_id;
            if($book_id > 0)
            {
                $this->_book_id = $book_id;
            }
        }

        public function setQuantity($quantity)
        {
            $this->_quantity = $quantity;
        }




    }


?>