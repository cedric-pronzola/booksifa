<?php

    class BooksManager
    {
        private $_db;

        public function __construct($db)
        {
            $this->setDb($db);
        }

        public function setDB(PDO $db)
        {
            $this->_db = $db;
        }

        /**
         * Récupère les infos d'un livre en fonction de l'ID
         *
         * @param integer $id Attend l'id du livre en argument
         * @return object Retourne l'objet Books
         */
        public function get($id)
        {
            $id = (int) $id;

            $q = $this->_db->query('SELECT *
            FROM books
            WHERE book_id ='.$id);

            $donnees = $q->fetch(PDO::FETCH_ASSOC);

            return new Books($donnees);
        }

        /**
         * Récupère tous les livre et les range par date. du plus récent au plus ancien
         *
         * @return array Retoure un tableau d'objet
         */
        public function getList()
        {
            $books = [];

            $q = $this->_db->query('SELECT *
            FROM books
            ORDER BY release_date DESC');

            while($donnees = $q->fetch(PDO::FETCH_ASSOC))
            {
                $books[] = new Books($donnees);
            }

            return $books;
        }

        /**
         * Récupère tous les livre sans les ranger
         *
         * @return array Retoure un tableau d'objet
         */
        public function getListNoOrder()
        {
            $books = [];

            $q = $this->_db->query('SELECT *
            FROM books');

            while($donnees = $q->fetch(PDO::FETCH_ASSOC))
            {
                $books[] = new Books($donnees);
            }

            return $books;
        }

        /**
         * Réupère les 3 meilleures ventes
         *
         * @return array Retoure un tableau d'objet
         */
        public function bestSeller()
        {
            $books = [];

            $q = $this->_db->query('SELECT books.*, command_content.*,
            SUM(command_content.quantity) as sumof
            FROM books
            INNER JOIN command_content ON command_content.book_id = books.book_id
            GROUP BY command_content.book_id
            ORDER BY sumof DESC
            LIMIT 3');

            while($donnees = $q->fetch(PDO::FETCH_ASSOC))
            {
                $books[] = new Books($donnees);
            }

            return $books;
        }

        /**
         * Récupère tous les livres regroupés par catégorie
         *
         * @return array Retoure un tableau d'objet
         */
        public function categoryList()
        {
            $books = [];

            $q = $this->_db->query('SELECT *
            FROM books
            GROUP BY category');

            while($donnees = $q->fetch(PDO::FETCH_ASSOC))
            {
                $books[] = new Books($donnees);
            }

            return $books;
        }

        /**
         * Fait un tri en fonction des arguments renseignés
         *
         * @param string $format format du livre (Grand ou poche)
         * @param string $sort Par quel attribut trier
         * @param string $order Type de tri (ASC ou DESC)
         * @param string $category Catégorie du livre
         * @return array Retoure un tableau d'objet
         */
        public function sortForm($format, $sort, $order, $category)
        {
            $books = [];

            if(in_array('grand_format', $format) AND in_array('poche', $format))
            {
                $format = 'all';
            }
            elseif(in_array('grand_format', $format))
            {
                $format = 'Grand Format';
            }
            elseif(in_array('poche', $format))
            {
                $format = 'Poche';
            }
            else
            {
                $format = 'all';
            }

            if($format == 'all' AND $category == 'all_cat')
            {
                $reqForm = $this->_db->prepare('SELECT books.*, authors.name
                FROM books
                INNER JOIN authors ON authors.author_id = books.author_id
                ORDER BY ' .$sort. ' ' .$order);
                $reqForm->execute();
    
                while($donnees = $reqForm->fetch(PDO::FETCH_ASSOC))
                {
                    $books[] = new Books($donnees);
                }
    
                return $books;
            }

            if($format == 'all' AND $category != 'all_cat')
            {
                $reqForm = $this->_db->prepare('SELECT books.*, authors.name
                FROM books
                INNER JOIN authors ON authors.author_id = books.author_id
                WHERE category = :category
                ORDER BY ' .$sort. ' ' .$order);
                $reqForm->bindValue(':category', $category);
                $reqForm->execute();
    
                while($donnees = $reqForm->fetch(PDO::FETCH_ASSOC))
                {
                    $books[] = new Books($donnees);
                }
    
                return $books;
            }

            if($format != 'all' AND $category == 'all_cat')
            {
                $reqForm = $this->_db->prepare('SELECT books.*, authors.name
                FROM books
                INNER JOIN authors ON authors.author_id = books.author_id
                WHERE format = :format
                ORDER BY ' .$sort. ' ' .$order);
                $reqForm->bindValue(':format', $format);
                $reqForm->execute();
    
                while($donnees = $reqForm->fetch(PDO::FETCH_ASSOC))
                {
                    $books[] = new Books($donnees);
                }
    
                return $books;
            }

            if($format != 'all' AND $category != 'all_cat')
            {
                $reqForm = $this->_db->prepare('SELECT books.*, authors.name
                FROM books
                INNER JOIN authors ON authors.author_id = books.author_id
                WHERE format = :format AND category = :category
                ORDER BY ' .$sort. ' ' .$order);
                $reqForm->bindValue(':format', $format);
                $reqForm->bindValue(':category', $category);
                $reqForm->execute();
    
                while($donnees = $reqForm->fetch(PDO::FETCH_ASSOC))
                {
                    $books[] = new Books($donnees);
                }
    
                return $books;
            }
        }
    }

?>