<?php

    class AuthorsManager
    {
        private $_db;

        public function __construct($db)
        {
            $this->setDb($db);
        }

        /**
         * Permet de récupérer les informations concernant l'auteur
         *
         * @param integer $bookId = ID du livre
         * @return object Authors
         */
        public function getByBookId($bookId)
        {
            $bookId = (int) $bookId;

            $q = $this->_db->query('SELECT a.author_id, a.name, a.sexe, a.birth_date, a.biography
            FROM authors a
            INNER JOIN books b ON b.author_id = a.author_id 
            WHERE book_id ='.$bookId);

            $donnees = $q->fetch(PDO::FETCH_ASSOC);

            return new Authors($donnees);
        }

        /**
         * Permet de récupérer les informations concernant l'auteur
         *
         * @param integer $id = ID de l'auteur
         * @return object Authors
         */
        public function getAuthorId($id)
        {
            $id = (int) $id;

            $q = $this->_db->query('SELECT *
            FROM authors
            WHERE author_id = '.$id);

            $donnees = $q->fetch(PDO::FETCH_ASSOC);

            return new Authors($donnees);
        }

        /**
         * Réupère tous les auteurs de la base de données
         *
         * @return void
         */
        public function getList()
        {
            $authors = [];

            $q = $this->_db->query('SELECT author_id, name, sexe, birth_date, biography, img
            FROM authors
            ORDER BY name');

            while($donnees = $q->fetch(PDO::FETCH_ASSOC))
            {
                $authors[] = new Authors($donnees);
            }

            return $authors;
        }

        public function setDB(PDO $db)
        {
            $this->_db = $db;
        }
    }


?>