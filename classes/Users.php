<?php

    class Users
    {
        private $_user_id;
        private $_first_name;
        private $_last_name;
        private $_email;
        private $_password;
        private $_phone;
        private $_address;
        private $_zip_code;
        private $_city;
        private $_country;
        private $_random;

        public function __construct(array $donnees)
        {
            $this->hydrate($donnees);
        }

        public function hydrate($donnees)
        {
            foreach($donnees as $key => $value)
            {
                $method = 'set'.ucfirst($key);

                if(method_exists($this, $method))
                {
                    $this->$method($value);
                }
            }
        }

        /**
         * Getter
         *-----------
         * 
         */

        public function user_id()
        {
            return $this->_user_id;
        }
        public function first_name()
        {
            return $this->_first_name;
        }
        public function last_name()
        {
            return $this->_last_name;
        }
        public function email()
        {
            return $this->_email;
        }
        public function password()
        {
            return $this->_password;
        }
        public function phone()
        {
            return $this->_phone;
        }
        public function address()
        {
            return $this->_address;
        }
        public function zip_code()
        {
            return $this->_zip_code;
        }
        public function city()
        {
            return $this->_city;
        }
        public function country()
        {
            return $this->_country;
        }
        public function random()
        {
            return $this->_random;
        }

        /**
         * Setter
         *--------
         */

        public function setUser_id($user_id)
        {
            $user_id = (int) $user_id;
            if($user_id > 0)
            {
                $this->_user_id = $user_id;
            }
        }
        public function setFirst_name($first_name)
        {
            if(is_string($first_name))
            {
                $this->_first_name = $first_name;
            }
        }
        public function setLast_name($last_name)
        {
            if(is_string($last_name))
            {
                $this->_last_name = $last_name;
            }
        }
        public function setEmail($email)
        {
            if(is_string($email))
            {
                $this->_email = $email;
            }
        }
        public function setPassword($password)
        {
            if(is_string($password))
            {
                $this->_password = $password;
            }
        }
        public function setPhone($phone)
        {
            if(is_string($phone))
            {
                $this->_phone = $phone;
            }
        }
        public function setAddress($address)
        {
            if(is_string($address))
            {
                $this->_address = $address;
            }
        }
        public function setZip_code($zip_code)
        {
            if(is_string($zip_code))
            {
                $this->_zip_code = $zip_code;
            }
        }
        public function setCity($city)
        {
            if(is_string($city))
            {
                $this->_city = $city;
            }
        }
        public function setCountry($country)
        {
            if(is_string($country))
            {
                $this->_country = $country;
            }
        }

        public function setRandom($random)
        {
            $random = (int) $random;
            if($random > 0)
            {
                $this->_random = $random;
            }
        }

    }


?>