<?php

    class Commands
    {
        private $_num_command;      // auto_increment in command, FOREIGN in command_content
        private $_user_id;          // command
        private $_date_command;     // command
        private $_total_price;      // command
        private $_random;           // command
        private $_mode;             // command

        public function __construct(array $donnees)
        {
            $this->hydrate($donnees);
        }
    
        public function hydrate(array $donnees)
        {
            foreach($donnees as $key => $value)
            {
                $method = 'set'.ucfirst($key);
    
                if(method_exists($this, $method))
                {
                    $this->$method($value);
                }
            }
        }

        /**
         * Getters
         * 
         */
        public function num_command()
        {
            return $this->_num_command;
        }
        public function user_id()
        {
            return $this->_user_id;
        }
        public function date_command()
        {
            return $this->_date_command;
        }
        public function total_price()
        {
            return $this->_total_price;
        }
        public function random()
        {
            return $this->_random;
        }
        public function mode()
        {
            return $this->_mode;
        }

        /**
         * Setters
         * 
         */
        public function setNum_command($num_command)
        {
            $num_command = (int) $num_command;
            if($num_command > 0)
            {
                $this->_num_command = $num_command;
            }
        }

        public function setUser_id($user_id)
        {
            $user_id = (int) $user_id;
            if($user_id > 0)
            {
                $this->_user_id = $user_id;
            }
        }

        public function setDate_command($date_command)
        {
            $this->_date_command = $date_command;
        }

        public function setTotal_price($total_price)
        {
            $this->_total_price = $total_price;
        }

        public function setRandom($random)
        {
            $this->_random = $random;
        }

        public function setMode($mode)
        {
            $this->_mode = $mode;
        }

    }




?>