<?php

    class UsersManager
    {
        private $_db;

        public function __construct($db)
        {
            $this->setDb($db);
        }

        public function setDB(PDO $db)
        {
            $this->_db = $db;
        }

        /**
         * Récupère les informations de l'utilisateur en fonctio nde son id
         *
         * @param integer $id Attend l'id de l'utilisateur en argument
         * @return object Retourne un objet Users
         */
        public function get($id)
        {
            $id = (int) $id;

            $q = $this->_db->query('SELECT * FROM users
            WHERE user_id = '.$id);

            $donnees = $q->fetch(PDO::FETCH_ASSOC);

            return new Users($donnees);
        }

        /**
         * Récupère les informations de l'utilisateur en fonction de la valeur de random
         *
         * @param integer $random Attend la valeur de random en argument
         * @return object Retourne un objet Users
         */
        public function getByRandom($random)
        {
            $random = (int) $random;

            $q = $this->_db->query('SELECT * FROM users
            WHERE random = '.$random);

            $donnees = $q->fetch(PDO::FETCH_ASSOC);

            return new Users($donnees);
        }

        /**
         * Ajoute un utilisateur dans la base de données
         *
         * @param Users $user Attend un objet Users en argument
         * @return void
         */
        public function add(Users $user)
        {

            $q = $this->_db->prepare('INSERT INTO users(first_name, last_name, email, password, phone, address, zip_code, city, country, random)
            VALUES(:first_name, :last_name, :email, :password, :phone, :address, :zip_code, :city, :country, :random)');

            $q->bindValue(':first_name', $user->first_name());
            $q->bindValue(':last_name', $user->last_name());
            $q->bindValue(':email', $user->email());
            $q->bindValue(':password', $user->password());
            $q->bindValue(':phone', $user->phone());
            $q->bindValue(':address', $user->address());
            $q->bindValue(':zip_code', $user->zip_code());
            $q->bindValue(':city', $user->city());
            $q->bindValue(':country', $user->country());
            $q->bindValue(':random', $user->random());


            $q->execute();
        }

        /**
         * Permet de supprimé un utilisateur de la base de données
         *
         * @param Users $user Attend un objet Users en argument
         * @return void
         */
        public function delete(Users $user)
        {
            $this->_db->exec('DELETE FROM users
            WHERE id = '.$user->user_id());
        }

        /**
         * Met à jour les informations de l'utilisateur
         *
         * @param Users $user Attend un objet Users en argument
         * @return void
         */
        public function update(Users $user)
        {
            $q = $this->_db->prepare('UPDATE users
            SET first_name = :first_name, last_name = :last_name, email = :email,
            password = :password, phone = :phone, address = :address, zip_code = :zip_code, city = :city
            WHERE user_id = :id');

            $q->bindValue(':first_name', $user->first_name(), PDO::PARAM_STR);
            $q->bindValue(':last_name', $user->last_name(), PDO::PARAM_STR);
            $q->bindValue(':email', $user->email(), PDO::PARAM_STR);
            $q->bindValue(':password', $user->password(), PDO::PARAM_STR);
            $q->bindValue(':phone', $user->phone() , PDO::PARAM_STR);
            $q->bindValue(':address', $user->address(), PDO::PARAM_STR);
            $q->bindValue(':zip_code', $user->zip_code(), PDO::PARAM_STR);
            $q->bindValue(':city', $user->city(), PDO::PARAM_STR);
            $q->bindValue(':id', $user->user_id(), PDO::PARAM_INT);

            $q->execute();

        }

        /**
         * Vérifie si le mail entré au moment de l'inscription existe déjà
         *
         * @param Object $user Attend l'objet Users en argumemt
         * @return void
         */
        public function verifyAccount($user)
        {
            try
            {
                $verifEmail = $this->_db->prepare('SELECT email
                FROM users
                WHERE email = :email');

                $verifEmail->bindValue(':email', $user->email());
                $verifEmail->execute();

                $verifEmail->fetch(PDO::FETCH_ASSOC);

                if($verifEmail->rowCount())
                {
                    throw new verifAccount();
                }
                else
                {
                    $this->add($user);

                    $expire = time() + 60 * 60 * 24 * 7;
    
                    setcookie('register', 'log_now', $expire, "/");

                }

            }
            catch(verifAccount $e)
            {
                echo "mail déja utilisé";
            }
        }

        /**
         * Vérifie le mail et le mot de passe au moment du log in
         *
         * @param string $email Attend une adresse mail en argument
         * @param string $password Attend le mot en argument
         * @return void
         */
        public function verifyEmail($email, $password)
        {
            try
            {
                $login = false;
                $q = $this->_db->prepare('SELECT user_id, email, password, first_name, random
                FROM users
                WHERE email = :email');
                $q->bindValue(':email', $email, PDO::PARAM_STR);
                $q->execute();
                $q->setFetchMode(PDO::FETCH_ASSOC);
    
                if(!$q->rowCount())                    // s'il n'y a aucune réponse
                {
                    throw new verifMail();
                }
    
                foreach($q as $row)
                {
                    if($row['email'] == $email && $row['password'] == $password)
                    {
                        $login = true;
                        $first_name = $row["first_name"];
                        $user_id = $row["user_id"];
                        $random = $row["random"];
                    }
                    elseif($row['email'] == $email && $row['password'] != $password)
                    {
                        throw new verifPassword();
                    }
                }

                if($login = true)
                {
                    $expire = time() + 60 * 60 * 24 * 7;
    
                    setcookie('connectToBook', $random, $expire, "/");
                }
            }
            catch(verifMail $e)
            {
                $expire = time() + 60 * 60 * 24 * 7;
    
                setcookie('wrong_email', $email, $expire, "/");
            }
            catch(verifPassword $e)
            {
                $expire = time() + 60 * 60 * 24 * 7;
    
                setcookie('wrong_password', $email, $expire, "/");
            }
        }
    }



?>