/* Cookie
function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

createCookie('bookifa', 'test1', 1);*/



$(function () {




	// variables pour stocker le nombre d'articles et leurs noms
	var inCartItemsNum;
	var cartArticles;
	let userID = getCookies('connectToBook');
	if(userID)
	{
		userID = getCookies('connectToBook');
	}
	else
	{
		userID = 0;
	}

	var timeout;

	$('#cart').on({
	mouseenter: function() {
		$('#cart-dropdown').show();
	},
	mouseleave: function() {
		timeout = setTimeout(function() {
		$('#cart-dropdown').hide();
		}, 200);
	}
	});

	// laisse le contenu ouvert à son survol
	// le cache quand la souris sort
	$('#cart-dropdown').on({
	mouseenter: function() {
		clearTimeout(timeout);
	},
	mouseleave: function() {
		$('#cart-dropdown').hide();
	}
	});

	/**
	 * 
	 * @param {string} cname nom du cookie
	 * @param {string} cvalue valeur du cookie
	 * @param {integer} exdays nombre de jour de validité du cookie
	 */
	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+d.toUTCString();
	  
		// règle le pb des caractères interdits
		/*if ('btoa' in window) {
		  cvalue = btoa(cvalue);
		}*/
	  
		document.cookie = cname + "=" + cvalue + "; " + expires+';path=/';
	}

	/**
	 * 
	 * @param {integer} inCartItemsNum nombre d'articles
	 * @param {string} cartArticles	nom des articles
	 */
	function saveCart(inCartItemsNum, cartArticles) {
	setCookie('inCartItemsNum', inCartItemsNum, 5);
	setCookie('cartArticles', JSON.stringify(cartArticles), 5);
	}

	/**
	 * 
	 * @param {string} cname nom du cookie
	 */
	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
	  
		for(var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c[0] == ' ') {
				c = c.substring(1);
			}
		
			if (c.indexOf(name) != -1) 
			{
				/*if ('btoa' in window) 
				{
					return atob(c.substring(name.length,c.length));
				}
				else 
				{*/
					return c.substring(name.length,c.length);
				//}
			}
		}
		return false;
	}

	// affiche/cache les éléments du panier selon s'il contient des produits
	function cartEmptyToggle() {
		if (inCartItemsNum > 0) {
		$('#cart-dropdown .hidden').removeClass('hidden');
		$('#empty-cart-msg').hide();
		}
	
		else {
		$('#cart-dropdown .go-to-cart').addClass('hidden');
		$('#empty-cart-msg').show();
		}
	}
	
	// récupère les informations stockées dans les cookies
	inCartItemsNum = parseInt(getCookie('inCartItemsNum') ? getCookie('inCartItemsNum') : 0);
	cartArticles = getCookie('cartArticles') ? JSON.parse(getCookie('cartArticles')) : [];
	
	cartEmptyToggle();
	
	// affiche le nombre d'article du panier dans le widget
	$('.in-cart-items-num').html(inCartItemsNum);
	
	// hydrate le panier
	var items = '';
	cartArticles.forEach(function(v) {
		items += '<li id="'+ v.id +'"><a href="'+ v.url +'">'+ v.name +'<br><small>Quantité : <span class="qt">'+ v.qt +'</span></small></a></li>';
	});
	
	$('#cart-dropdown').prepend(items);

	// click bouton ajout panier
	$('.add-to-cart').click(function() {

		// récupération des infos du produit
		var $this = $(this);
		var id = $this.attr('data-id');
		var name = $this.attr('data-name');
		var price = $this.attr('data-price');
		var weight = $this.attr('data-weight');
		var url = $this.attr('data-url');
		var qt = parseInt($(this).next().next().children('input').val());

		inCartItemsNum += qt;

		console.log(qt);

		

		// mise à jour du nombre de produit dans le widget
		$('.in-cart-items-num').html(inCartItemsNum);
	
		var newArticle = true;

	
		// vérifie si l'article est pas déjà dans le panier
		cartArticles.forEach(function(v) {
		// si l'article est déjà présent, on incrémente la quantité
		if (v.id == id) {
			newArticle = false;
			v.qt += qt;
			$('#'+ id).html('<a href="'+ url +'">'+ name +'<br><small>Quantité : <span class="qt">'+ v.qt +'</span></small></a>');
		}
		});
	
		// s'il est nouveau, on l'ajoute
		if (newArticle) {
		$('#cart-dropdown').prepend('<li id="'+ id +'"><a href="'+ url +'">'+ name +'<br><small>Quantité : <span class="qt">'+ qt +'</span></small></a></li>');
	
		cartArticles.push({
			id: id,
			name: name,
			price: price,
			weight: weight,
			qt: qt,
			url: url
		});
		}
	
		// sauvegarde le panier
		saveCart(inCartItemsNum, cartArticles);
	
		// affiche le contenu du panier si c'est le premier article
		cartEmptyToggle();
	});


  // si on est sur la page ayant pour url monsite.fr/panier/
	if (window.location.pathname == '/librairie-poo/pages/cart.php') 
	{
		var items = '';
		var subTotal = 0;
		var total;
		var weight = 0;

	
		/* on parcourt notre array et on créé les lignes du tableau pour nos articles :
		* - Le nom de l'article (lien cliquable qui mène à la fiche produit)
		* - son prix
		* - la dernière colonne permet de modifier la quantité et de supprimer l'article
		*
		* On met aussi à jour le sous total et le poids total de la commande
		*/
		cartArticles.forEach(function(v) {
		// opération sur un entier pour éviter les problèmes d'arrondis
		var itemPrice = v.price.replace(',', '.') * 1000;
		items += '<tr data-id="'+ v.id +'">\
				<td><a class="text-white" href="'+ v.url +'">'+ v.name +'</a></td>\
				<td>'+ v.price +'€</td>\
				<td><span class="qt mr-3 font-weight-bold h5 text-danger">'+ v.qt +'</span> <a title="Ajouter" class="qt-plus fas fa-plus mr-3"></a> <a title="Enlever" class="qt-minus fas fa-minus mr-5"></a>  \
				<a title="Supprimer l\'élément" class="delete-item fas fa-trash-alt fa-2x"></a></td><td>Gratuit</td></tr>';
		subTotal += v.price.replace(',', '.') * v.qt;
		weight += v.weight * v.qt;
		});

	
		// on reconverti notre résultat en décimal
		//subTotal = subTotal / 1000;
	
		// On insère le contenu du tableau et le sous total
		$('#cart-tablebody').empty().html(items);
		$('.subtotal').html(subTotal.toFixed(2).replace('.', ','));
	
		// lorsqu'on clique sur le "+" du panier
		$('.qt-plus').on('click', function() {
		var $this = $(this);
	
		// récupère la quantité actuelle et l'id de l'article
		var qt = parseInt($this.prevAll('.qt').html());
		var id = $this.parent().parent().attr('data-id');
		var artWeight = parseInt($this.parent().parent().attr('data-weight'));
	
		// met à jour la quantité et le poids
		inCartItemsNum += 1;
		weight += artWeight;
		$this.prevAll('.qt').html(qt + 1);
		$('.in-cart-items-num').html(inCartItemsNum);
		$('#'+ id + ' .qt').html(qt + 1);
	
		// met à jour cartArticles
		cartArticles.forEach(function(v) {
			// on incrémente la qt
			if (v.id == id){
				v.qt += 1;
	
				// récupération du prix
				// on effectue tous les calculs sur des entiers
				subTotal = ((subTotal * 1000) + (parseFloat(v.price.replace(',', '.')) * 1000)) / 1000;
			}
		});
	
		// met à jour la quantité du widget et sauvegarde le panier
		$('.subtotal').html(subTotal.toFixed(2).replace('.', ','));
		saveCart(inCartItemsNum, cartArticles);
		});
	
		// quantité -
		$('.qt-minus').click(function() 
		{
			var $this = $(this);
			var qt = parseInt($this.prevAll('.qt').html());
			var id = $this.parent().parent().attr('data-id');
			var artWeight = parseInt($this.parent().parent().attr('data-weight'));
		
			if (qt > 1) 
			{
				// maj qt
				inCartItemsNum -= 1;
				weight -= artWeight;
				$this.prevAll('.qt').html(qt - 1);
				$('.in-cart-items-num').html(inCartItemsNum);
				$('#'+ id + ' .qt').html(qt - 1);
		
				cartArticles.forEach(function(v) {
					// on décrémente la qt
					if (v.id == id) {
						v.qt -= 1;
		
						// récupération du prix
						// on effectue tous les calculs sur des entiers
						subTotal = ((subTotal * 1000) - (parseFloat(v.price.replace(',', '.')) * 1000)) / 1000;
					}
				});
		
				$('.subtotal').html(subTotal.toFixed(2).replace('.', ','));
				saveCart(inCartItemsNum, cartArticles);
			}
		});
  
		// suppression d'un article
		$('.delete-item').click(function() {
			var $this = $(this);
			var qt = parseInt($this.prevAll('.qt').html());
			var id = $this.parent().parent().attr('data-id');
			var artWeight = parseInt($this.parent().parent().attr('data-weight'));
			var arrayId = 0;
			var price;
		
			// maj qt
			inCartItemsNum -= qt;
			$('.in-cart-items-num').html(inCartItemsNum);
		
			// supprime l'item du DOM
			$this.parent().parent().hide(600);
			$('#'+ id).remove();
		
			cartArticles.forEach(function(v) {
				// on récupère l'id de l'article dans l'array
				if (v.id == id) {
					// on met à jour le sous total et retire l'article de l'array
					// as usual, calcul sur des entiers
					var itemPrice = v.price.replace(',', '.') * 1000;
					subTotal -= (itemPrice * qt) / 1000;
					weight -= artWeight * qt;
					cartArticles.splice(arrayId, 1);
		
					return false;
				}
				arrayId++;
			});
		
			$('.subtotal').html(subTotal.toFixed(2).replace('.', ','));
			saveCart(inCartItemsNum, cartArticles);
			cartEmptyToggle();
		});

		if(inCartItemsNum == 0)
		{
			$('#del_all_cart').hide();
		}


	}


	function getCookies(cname) 
	{
		var name = cname + "=";
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for(var i = 0; i <ca.length; i++) {
		  var c = ca[i];
		  while (c.charAt(0) == ' ') {
			c = c.substring(1);
		  }
		  if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		  }
		}
		return "";
	}

	let arrayBook = [];
	
	for (const iterator of cartArticles) 
	{
		arrayBook.push({user_id : userID,
					book_id : iterator.id,
					quantity : iterator.qt})
	}

	$('#del_all_cart').click(function(){
		setCookie('inCartItemsNum', null, -1, "/");
		setCookie('cartArticles', null, -1, "/");
		document.location.href="./cart.php";

	});

	$('#createJsonFile').click(function(){
		$('#donloadJson').append("<button>")
	});

	$('#formJson').on('submit', function(e){
		e.preventDefault();

		$('#createJsonFile').attr('disabled', true);

		var request = $.ajax({
			url: './admin.php',
			type: 'POST',
			data: 'download'
		})

		request.done(function(downjason){
			$('#toDownloadJSON').fadeIn(400).show();
		})
		
	})

	$('#downloadJsonFile').click(function(){
		$('#toDownloadJSON').fadeOut(400).show();

		$('#createJsonFile').attr('disabled', false);

	})

	$('.add-to-cart').click(function(){
		$(this).mouseover();
		
		// after a slight 2 second fade, fade out the tooltip for 1 second
		$(this).next().animate({opacity: 0.9},{duration: 2000, complete: function(){
			$(this).fadeOut(1000);
		}});
		
	});
	
/**
	 * store the value of and then remove the title attributes from the
	 * abbreviations (thus removing the default tooltip functionality of
         * the abbreviations)
	 */
	$('.add-to-cart').each(function(){		
		
		$(this).data('title',$(this).attr('title'));
		$(this).removeAttr('title');
	
	});




        /**
	 * when abbreviations are mouseover-ed show a tooltip with the data from the title attribute
	 */	
	$('.add-to-cart').mouseover(function() {		
		
		// first remove all existing abbreviation tooltips
		$('.add-to-cart').next('.tooltip').remove();
		
		// create the tooltip
		$(this).after('<span class="tooltip">' + $(this).data('title') + '</span>');
		
		// position the tooltip 4 pixels above and 4 pixels to the left of the abbreviation
		var left = $(this).position().left + $(this).width() + 10;
		var top = $(this).position().top - 4;
		$(this).next().css({
			color: 'white',
			background: 'red',
			fontWeight: 'bold',
			border: '1px solid black'
		});
		$(this).next().css('left',left);
		$(this).next().css('top',top);				
		
	});
	
	/**
	 * when abbreviations are clicked trigger their mouseover event then fade the tooltip
	 * (this is friendly to touch interfaces)
	 */
	$('.add-to-cart').click(function(){
		
		$(this).mouseover();
		
		// after a slight 2 second fade, fade out the tooltip for 1 second
		$(this).next().animate({opacity: 0.9},{duration: 2000, complete: function(){
			$(this).fadeOut(1000);
		}});
		
	});
	
	/**
	 * Remove the tooltip on abbreviation mouseout
	 */
	$('.add-to-cart').mouseout(function(){
			
		$(this).next('.tooltip').remove();				

	});	

	$('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
	
});

