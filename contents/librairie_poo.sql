-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  sam. 27 avr. 2019 à 15:11
-- Version du serveur :  5.7.23
-- Version de PHP :  7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `librairie_poo`
--

-- --------------------------------------------------------

--
-- Structure de la table `authors`
--

DROP TABLE IF EXISTS `authors`;
CREATE TABLE IF NOT EXISTS `authors` (
  `author_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `sexe` varchar(10) NOT NULL,
  `birth_date` date NOT NULL,
  `biography` text NOT NULL,
  `img` text,
  PRIMARY KEY (`author_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `authors`
--

INSERT INTO `authors` (`author_id`, `name`, `sexe`, `birth_date`, `biography`, `img`) VALUES
(1, 'BRANCO Juan', 'Homme', '1989-08-26', 'Juan Branco est le fils du producteur de cinéma portugais Paulo Branco et de la psychanalyste espagnole Dolores López, et a deux sœurs et un frère. Il grandit dans un milieu intellectuel favorisé, au contact de nombreuses personnalités du cinéma français : « À la maison, il y avait toujours Raoul Ruiz ou Catherine Deneuve à dîner. Avec ces gens-là, tu as plutôt intérêt à avoir des choses à dire si tu ouvres la bouche ». Adolescent, il se rend régulièrement au Festival de Cannes.\r\nNé en Espagne dans la commune d\'Estepona, il grandit entre l\'Andalousie et le 5e et 6e arrondissement de Paris. Il devient notamment cavalier d\'endurance en 2004, se classant 15e aux championnats du monde de Compiègne en 2007 sous bannière espagnole.\r\nIl est naturalisé français en 2010.\r\nIl perçoit depuis 2018 le revenu de solidarité active (RSA).\r\nIl prétend être fiché S (individu mettant en cause la sûreté de l\'Etat) dans une interview pour Le Figaro. ', 'juan_branco.jpg'),
(2, 'KAMTO Maurice', 'Homme', '1954-08-02', 'Maurice KAMTO est né le 14 Février 1954 à Bafoussam, au Cameroun. Il obtient une licence en droit public à l’Université de Yaoundé. En 1980, il obtient un diplôme d’études approfondies de droit public fondamental et un diplôme d’études approfondies de droit international à la faculté de droit de l’Université de Nice. En 1982, il est diplômé de l’institut international d’administration publique de Paris. En 1983, il obtient un doctorat d’Etat en droit à la faculté de droit de Nice. En 1988, il reçoit le Prix de l’Académie des sciences d’outre-mer. Il est agrégé des Facultés françaises de droit la même année. Il est successivement professeur aux universités de Yaoundé II en fonction depuis le 02 Février 1988, de N’Gaoundéré et, depuis 1994, à l’université de Yaoundé. Il enseigne également à l’Institut des Relations Internationales (IRIC), et à l’École nationale d’administration et de magistrature (ENAM) de Yaoundé. Il est régulièrement invité par plusieurs universités occidentales et africaines. Il est membre du jury du concours d’entrées de plusieurs institutions.', 'maurice_kamto.jpg'),
(3, 'MERIENNE Patrick', 'Homme', '1974-05-03', 'Patrick Mérienne est géographe et cartographe, auteur de guides de randonnées.', 'patrick_merienne.jpg'),
(4, 'DESTAL Mathias', 'Homme', '1978-12-03', 'Au service investigation de Marianne jusqu\'en 2017, il travaille désormais en indépendant. Il est l\'auteur de plusieurs enquêtes sur l\'extrême droite radicale et co-auteur du livre \"Marine est au courant de tout - Argent secret, financements, hommes de l\'ombre : Enquête sur Marine Le Pen\".', 'mathias_destal.jpg'),
(5, 'GRACIET Catherine', 'Femme', '1980-08-03', 'Catherine Graciet est une journaliste française spécialiste du Maghreb.', 'catherine_graciet.jpg'),
(6, 'JAUVERT Vincent', 'Homme', '1959-03-05', 'Vincent Jauvert est un journaliste français né en 1959.\r\nGrand reporter au service Monde du Nouvel Observateur, il est l\'auteur de Les Intouchables d’État, bienvenue en Macronie (Robert Laffont, 2018), La Face Cachée du Quai d\'Orsay (Robert Laffont, 2016) et L\'Amérique contre De Gaulle, histoire secrète 1961-1969 (Seuil, 2000).\r\nIl est également co-auteur de deux documentaires télévisés : L\'Ami Américain, tiré de son premier ouvrage (FR3, 2001 avec Patrick Jeudy) et Révolution, Mode d\'Emploi (Arte, 2007 avec Tania Rackhmanova).\r\nIl tient un blog intitulé « Affaires étrangères ».', 'vincent_jauvert.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `books`
--

DROP TABLE IF EXISTS `books`;
CREATE TABLE IF NOT EXISTS `books` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text,
  `release_date` date NOT NULL,
  `edition` varchar(30) NOT NULL,
  `category` varchar(20) NOT NULL,
  `format` varchar(20) NOT NULL,
  `price` decimal(19,2) DEFAULT NULL,
  `img` text,
  PRIMARY KEY (`book_id`),
  KEY `author_id` (`author_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `books`
--

INSERT INTO `books` (`book_id`, `author_id`, `title`, `description`, `release_date`, `edition`, `category`, `format`, `price`, `img`) VALUES
(1, 1, 'Crépuscule', 'L\'avocat et journaliste publie un réquisitoire contre la politique d\'Emmanuel Macron, d\'abord diffusé sur Internet en marge du mouvement des gilets jaunes. Remanié et chapitré différemment, le texte se présente sous la forme d\'une enquête menée auprès d\'amis et de proches du président de la République autour des conditions de son accession à l\'Elysée.', '2019-03-21', 'Au diable vauvert', 'Enquête', 'Grand Format', '19.00', 'crepuscule.jpg'),
(2, 1, 'Contre Macron', '« Le Macronisme est une nouvelle variante du fascisme, et il nous faudra avoir la plus grande attention à la façon de débrancher ces êtres de nos institutions au moment du changement démocratique nécessaire et qu\'ils chercheront compulsivement à éviter. » Telle est la thèse de Juan Branco, normalien et docteur en droit, conseiller juridique de Julian Assange et de Wikileaks, spécialisé dans les violences politiques et de masse. Ce texte montre comment, dès les premiers jours de son mandat, se dessinait chez Emmanuel Macron une pratique du pouvoir dangereuse pour la démocratie, ancrée dans une histoire politique éloignée des préceptes auxquels sa rhétorique donnait l\'impression d\'adhérer. ', '2019-01-12', 'Divergences', 'Politique', 'Poche', '13.00', 'contre_macron.jpg'),
(3, 2, 'L\'Urgence de la pensée', 'Publié en 1993, cet essai philosophico-politique conserve toute sa valeur. Pour reprendre Hindrich ASSONGO On peut si l’on veut, questionner et condamner l’acerbe et objectif Maurice Kamto, pour avoir siégé au gouvernement dont il a combattu l’irrationalité dans ses écrits. On pourrait, si l’on souhaite, s’interroger sur sa vraie ou fausse appartenance au sérail du Cameroun. Au terme de cet exercice de fouine inutile, il va rester une constance. Le message de son essai politico-philosophique, « L’Urgence de la pensée » garde toute sa validité', '1998-07-21', 'Menaibuc Eds', 'Histoire', 'Poche', '10.67', 'urgence.jpg'),
(4, 3, 'Atlas mondial', 'Cet atlas est une mise à jour. Parce qu\'une bonne compréhension de l\'actualité nécessite une information précise, il fallait pour le monde un atlas à la fois complet, clair et synthétique. Un atlas qui va à l\'essentiel. Les événements majeurs qui ont fait l\'histoire récente de chaque pays sont ici rappelés. Accessible à tous, cet atlas accompagnera aussi vos enfants tout au long de leur scolarité.', '2014-08-22', 'Ouest France', 'Géographie', 'Grand Format', '5.00', 'atlas.jpg'),
(5, 4, '\"Marine est au courant de tout...\"', 'Argent secret, financements, hommes de l\'ombre... Marine Le Pen à l\'épreuve des faits. Au fil de plusieurs années d\'enquête, Marine Turchi et Mathias Destal ont plongé dans les eaux profondes du Front national afin d\'en remonter des vérités gênantes. De quoi mettre à mal la stratégie dite de \" dédiabolisation \" du parti frontiste. Ainsi le Front national serait le \"parti du peuple\" face à \"la droite et la gauche du fric\" ? Les auteurs démontrent, témoignages et documents exclusifs à l\'appui, que Marine Le Pen, sa famille et certains de ses proches, aiment l\'argent. Parfois jusqu\'à l\'excès. Ainsi le Front national avancerait dans l\'arène politique \"tête haute et mains propres \" quand les autres formations se vautreraient dans les petits arrangements et les financements occultes ? Ce livre explique pourquoi le FN, sa présidente ou son entourage sont au coeur de plusieurs affaires embarrassantes. Ainsi le Front national serait désormais un parti \"républicain\" tout à fait présentable ? Non seulement le FN n\'a pas chassé ses vieux démons, mais les auteurs dévoilent les profils plus qu\'inquiétants des hommes de l\'ombre qui évoluent dans le premier cercle de la présidente frontiste. Pour découvrir l\'autre visage de Marine Le Pen.', '2017-03-16', 'Editions Flammarion', 'Enquête', 'Grand Format', '21.00', 'marine.jpg'),
(6, 5, 'Sarkozy-Kadhafi. Histoire secrète d\'une trahison', 'Décembre 2007 : le président Sarkozy accueille en grande pompe le colonel Kadhafi à Paris. Mars 2011 : la France entre en guerre en Libye. Que s\'est-il passé entre ces deux dates ?\r\nQuelques mots résument à eux seuls l\'histoire des relations tumultueuses entre les deux chefs d\'État : \" financement politique \", \" pétrole et gaz \", \" ventes d\'armes \". Et ce livre révèle d\'abord les conditions et le modus operandi du financement libyen de la campagne présidentielle de Nicolas Sarkozy en 2007. S\'annonce alors le règne des hommes de l\'ombre évoluant entre Paris et Tripoli –; Ziad Takieddine, Alexandre Djouhri et le très secret Souheil Rached –;, dont les agissements pèseront pendant des années sur la politique étrangère de la France. Jusqu\'au fameux retournement.\r\nCes faits historiques, et bien d\'autres encore, sont établis sur la base de témoignages inédits. Les témoins clés de ce récit s\'expriment à visage découvert, au péril de leur vie lorsqu\'ils sont libyens et recherchés. Mais plusieurs Français aussi, anciens diplomates et marchands d\'armes en activité, tombent ici le masque.\r\nEn exclusivité également, des documents jamais publiés sur les négociations de ventes d\'armes françaises à la Libye de 1999 à 2010. Mais aussi les dessous des tractations secrètes pour assurer au Rafale le marché libyen. Enfin, des informations inédites sur l\'opération commando dirigée par Cécilia Sarkozy pour libérer les infirmières bulgares en 2007 et sur les dessous politiques et financiers de l\'affaire. Sans oublier d\'édifiants éclairages sur le jeu discret mais obstiné du Qatar tout au long de ces années.\r\nCatherine Graciet est journaliste. Elle a publié, avec Nicolas Beau, Quand le Maroc sera islamiste (2006) et La Régente de Carthage (2009), et, avec Eric Laurent, Le Roi prédateur (2012).', '2013-09-04', 'Le Seuil', 'Enquête', 'Grand Format', '19.50', 'sarkozy_kadhafi.jpg'),
(7, 6, 'La face cachée du quai d\'Orsay : Enquête sur un ministère à la dérive', 'C\'est le ministère le plus prestigieux de la République - le gardien de la \" grandeur \" de la France. Pourtant, aujourd\'hui, le Quai d\'Orsay est à la dérive. Pourquoi la guerre des clans y fait rage ? Jusqu\'où la corruption et le népotisme gangrènent-ils l\'institution ? Le ministère des Affaires étrangères est-il devenu outrageusement pro-américain ? Pourquoi tant de ministres incompétents se sont-ils succédé à sa tête ? Le Quai a-t-il encore les moyens financiers de défendre les intérêts de la France ? Reposant sur une cinquantaine de témoignages et des documents confidentiels, l\'ouvrage passe au crible les secrets inavouables du Quai : les turpitudes de ce grand ambassadeur de gauche ; les affaires de détournement de fonds étouffées ; le lobbying forcené des néoconservateurs au sein du ministère qui s\'appellent eux-mêmes \" la secte \" ; les accords secrets avec la DGSE ; le \" nettoyage \" du Quai par Laurent Fabius ; ou encore le bradage des \" bijoux de famille \", ces résidences qui ont fait la réputation de la France, etc.', '2017-04-12', 'J\'ai lu', 'Enquête', 'Grand Format', '6.70', 'face_cachee.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `commands`
--

DROP TABLE IF EXISTS `commands`;
CREATE TABLE IF NOT EXISTS `commands` (
  `num_command` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date_command` date NOT NULL,
  `total_price` decimal(10,2) DEFAULT NULL,
  `random` bigint(20) DEFAULT NULL,
  `mode` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`num_command`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `command_content`
--

DROP TABLE IF EXISTS `command_content`;
CREATE TABLE IF NOT EXISTS `command_content` (
  `num_command` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`num_command`,`book_id`),
  KEY `book_id` (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `phone` varchar(15) NOT NULL,
  `address` text NOT NULL,
  `zip_code` varchar(5) NOT NULL,
  `city` varchar(30) DEFAULT NULL,
  `country` varchar(10) DEFAULT NULL,
  `random` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `zip_code`, `city`, `country`, `random`) VALUES
(1, 'admin', 'admin', 'admin@admin.fr', '29b31abebb812b2e464810e44774cb50', '0645313131', 'no address', '57380', 'Faulquemont', 'France', 2070018967679),
(2, 'Cédric', 'PRONZOLA', 'c.pronzola@live.fr', 'a5541041f81c54f56aa192657da83f76', '0303030303', '2 rue du Quebec', '27200', 'Vernon', 'France', 1571217552438),
(3, 'Cédric20', 'PRONZOLA', 'adolphe.bassem@gmail.com', 'a5541041f81c54f56aa192657da83f76', '0645313131', '2 rue faefvaeva', '57380', 'Faulquemont', 'France', 906721204143);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `authors` (`author_id`);

--
-- Contraintes pour la table `commands`
--
ALTER TABLE `commands`
  ADD CONSTRAINT `commands_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Contraintes pour la table `command_content`
--
ALTER TABLE `command_content`
  ADD CONSTRAINT `command_content_ibfk_1` FOREIGN KEY (`num_command`) REFERENCES `commands` (`num_command`),
  ADD CONSTRAINT `command_content_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
